const path = require('path');

module.exports = {
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        }]
    },
    entry: './js/main.js',
    output: {
        path: path.resolve(__dirname, './js'),
        filename: "bundle.js",
    },

    mode: 'development'
};